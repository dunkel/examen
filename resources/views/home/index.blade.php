@extends('layouts.app')

@section('title', 'Buscador')

@section('content')
    <form id="search_form" name="search_form">
        <div class="form-group">
            <label for="magnitude">Magnitud</label>
            <input type="text" class="form-control" id="magnitude" name="magnitude" placeholder="Magnitud">
        </div>
        <div class="form-group">
            <label for="date_start">Fecha de inicio</label>
            <input type="text" class="form-control" id="date_start" name="date_start" placeholder="Fecha de inicio">
        </div>
        <div class="form-group">
            <label for="date_end">Fecha de término</label>
            <input type="text" class="form-control" id="date_end" name="date_end" placeholder="Fecha de término">
        </div>
        <button type="button" id="search" class="btn btn-success">Buscar</button>
        <button type="button" id="search_last" class="btn btn-primary">Última búsqueda</button>
    </form>
    <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
        <tr>
            <th>magnitud</th>
            <th>hora</th>
            <th>lugar</th>
            <th>datalle</th>
        </tr>
        </thead>
    </table>

    <!-- Modal -->
    <div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Detalle</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <table class="table table-striped table-bordered" style="width:100%">
                        <tr>
                            <td>magnitud</td>
                            <td><p id="d_mag"></p></td>
                        </tr>
                        <tr>
                            <th>hora</th>
                            <th><p id="d_time"></p></th>
                        </tr>
                        <tr>
                            <th>lugar</th>
                            <th><p id="d_place"></p></th>
                        </tr>
                    </table>
                    <div style="width: 100%; height: 400px;" id="map_canvas"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    @parent
    <script>
        var tableObj = "";
        var localStorage = window.localStorage;
        var map = null;
        var myMarker;
        var myLatlng;
        var Lonlng;
        var Latlng;
        if ("geolocation" in navigator){
            navigator.geolocation.getCurrentPosition(function(position){
                Lonlng = position.coords.latitude;
                Latlng = position.coords.longitude;
            });
        }
        $(document).ready(function () {
            jQuery.validator.addMethod("greaterThan", function (value, element, params) {
                if ($(params[0]).val() != '') {
                    if (!/Invalid|NaN/.test(new Date(value))) {
                        return new Date(value) > new Date($(params[0]).val());
                    }
                    return isNaN(value) && isNaN($(params[0]).val()) || (Number(value) > Number($(params[0]).val()));
                }
                ;
                return true;
            }, 'Debe ser mayor que la {1}.');

            var validator = $("#search_form").validate({
                meta: "validate",
                rules: {
                    magnitude: {
                        required: true,
                        number: true,
                    },
                    date_start: {
                        required: true
                    },
                    date_end: {
                        required: true,
                        greaterThan: ["#date_start", "Fecha de inicio"]
                    }
                },
            });
            $("#search_last").on("click", function () {
                var requestlast_request = localStorage.getItem('last');
                if (requestlast_request) {
                    var request = JSON.parse(requestlast_request)
                    $("#magnitude").val(request.minmagnitude);
                    $("#date_start").val(request.starttime);
                    $("#date_end").val(request.endtime);
                }
            });
            $("#search").on("click", function () {
                if ($("#search_form").valid()) {
                    var magnitude = $("#magnitude").val();
                    var date_start = $("#date_start").val();
                    var date_end = $("#date_end").val();
                    var request = {
                        starttime: date_start, endtime: date_end, minmagnitude: magnitude,
                        format: "geojson"
                    };
                    localStorage.setItem('last', JSON.stringify(request));
                    $.ajax({
                        method: "GET",
                        url: "https://earthquake.usgs.gov/fdsnws/event/1/query",
                        data: request
                    })
                        .done(function (data) {
                            var data_table = data.features.map(function (obj) {
                                var rObj = {};
                                const date = new Date(obj.properties.time);
                                rObj.mag = obj.properties.mag;
                                rObj.date = date.getDate() + "-" + (date.getMonth() + 1) + "-" + date.getFullYear();
                                rObj.time = date.getHours() + ":" + (date.getSeconds());
                                rObj.place = obj.properties.place;
                                rObj.detail = obj.properties.detail;
                                return rObj;
                            });

                            if (tableObj !== "") {
                                tableObj.clear();
                                tableObj.destroy();
                            }
                            tableObj = $('#example').DataTable({
                                "data": data_table,
                                columns: [
                                    {
                                        "data": "mag",
                                        "render": function (data, type, row, meta) {
                                            var style = "";
                                            if (data <= 4) {
                                                style = "text-success";
                                            }
                                            if (data > 4 && data <= 6) {
                                                style = "text-warning";
                                            }
                                            if (data > 6 && data <= 7) {
                                                style = "text-info";
                                            }

                                            if (data > 7) {
                                                style = "text-danger";
                                            }
                                            return "<p class='" + style + "'>" + data + "</p>";
                                        }
                                    },
                                    {"data": "time"},
                                    {"data": "place"},
                                    {
                                        "data": "detail",
                                        "render": function (data, type, row, meta) {
                                            return "<button class='btn btn-info detail_mag' data-href='" + data + "'>Detalle</button>";
                                        }
                                    }
                                ]
                            });
                        });
                }
            });
            $("table").on("click", ".detail_mag", function () {
                var url = $(this).data('href');
                $.get(url, function (data) {
                    const datetime = new Date(data.properties.time);
                    var mag = data.properties.mag;
                    var date = datetime.getDate() + "-" + (datetime.getMonth() + 1) + "-" + datetime.getFullYear();
                    var time = datetime.getHours() + ":" + (datetime.getSeconds());
                    var place = data.properties.place;
                    var place = data.geometry.coordinates;
                    $("#d_mag").html(mag);
                    $("#d_time").html(time);
                    $("#d_place").html(place);
                    initializeGMap(place[1], place[0]);
                    $('#exampleModal').modal('show');
                });
            });
        });

        function initializeGMap(lat, lng) {
            myLatlng = new google.maps.LatLng(lat, lng);

            var myOptions = {
                zoom: 12,
                zoomControl: true,
                center: myLatlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

            myMarker = new google.maps.Marker({
                position: myLatlng
            });
            myMarker.setMap(map);
            var myLatlng2 = new google.maps.LatLng(Lonlng, Latlng);
            myMarker = new google.maps.Marker({
                position: myLatlng2
            });
            myMarker.setMap(map);
            var dis = distance(lat, lng, Latlng, Lonlng, 'K');
            if(dis>300){
                alert("esta muy lejos de tu ubicación");
            }
        }

        function distance(lat1, lon1, lat2, lon2, unit) {
            if ((lat1 == lat2) && (lon1 == lon2)) {
                return 0;
            }
            else {
                var radlat1 = Math.PI * lat1/180;
                var radlat2 = Math.PI * lat2/180;
                var theta = lon1-lon2;
                var radtheta = Math.PI * theta/180;
                var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
                if (dist > 1) {
                    dist = 1;
                }
                dist = Math.acos(dist);
                dist = dist * 180/Math.PI;
                dist = dist * 60 * 1.1515;
                if (unit=="K") { dist = dist * 1.609344 }
                if (unit=="N") { dist = dist * 0.8684 }
                return dist;
            }
        }
    </script>
@endsection

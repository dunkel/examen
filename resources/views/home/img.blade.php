@extends('layouts.app')

@section('title', 'Index')
@section('content')
    <style>
        #img_all{
            background-image: url("{{asset('img/sismo.jpeg')}}");
            background-color: #cccccc; /* Used if the image is unavailable */
            height: 500px !important; /* You must set a specified height */
            background-position: center; /* Center the image */
            background-repeat: no-repeat; /* Do not repeat the image */
            background-size: cover; /* Resize the background image to cover the entire container */
        }
    </style>
    <div id="img_all">

    </div>
@endsection
@section('js')
    @parent
    <script>
        setTimeout(function () {
            console.log("hola");
            window.location.href ="{{route('search')}}";
        }, 5000);
    </script>
@endsection
